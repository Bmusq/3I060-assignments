
import java.io.*;
import java.util.StringTokenizer;  
//****************
//START: READ ONLY
//****************
public class Edit {
//****************
//END: READ ONLY
//****************
    public static int[][][] edit;
    public static final int nOP=4;
    public static int lenA;
    public static int lenB;
    public static String aS;
    public static String bS;


    public static void printEditTable(int[][][] edit){
        int i,j, k;
        for(i=0;i<edit.length;i++){
            for(j=0;j<edit[i].length;j++){
                for(k=0; k<edit[i][j].length; k++){
                    System.out.print(edit[i][j][k]+" ");
                }
            }
            System.out.println();
        }
        System.out.println("\n\n");
    }

    public static int fillTable(int[][][] edit, int i, int j, int k){
        int del, ins, sub, skip, min;

        if(i==0 && j==0){
            edit[i][j][k]=0; // Reach end of edit[][][k]
            return edit[i][j][k];
        }


        if(i==0  && j>1){
            edit[i][j][k]=lenA+lenB; //Operation impossible to lead
            return edit[i][j][k];
        }
        if(j==0  && i>1){
            edit[i][j][k]=lenA+lenB; //Operation impossible to lead
            return edit[i][j][k];
        }


        if(i == 0 && j==1 && k!=1){
            edit[i][j][k]=lenA+lenB; //Operation impossible to lead
            return edit[i][j][k];
        }

        if(j == 0 && i==1 && k!=2){
            edit[i][j][k]=lenA+lenB; //Operation impossible to lead
            return edit[i][j][k];     
        }

        switch(k){
            case 0:
            if(!(i==0 || j==0)){
                if(aS.charAt(i-1) != bS.charAt(j-1)){
                    edit[i][j][k]=lenA+lenB;
                    return edit[i][j][k];
                }
            }

                del=fillTable(edit, i-1, j-1, 2);
                ins=fillTable(edit, i-1, j-1, 1);
                sub=fillTable(edit, i-1, j-1, 3);
                skip=fillTable(edit, i-1, j-1, 0);
                
                min = minFour(del, ins, sub, skip);
                edit[i][j][k]= 0 + min;
                return edit[i][j][k];

            case 1:
                del=fillTable(edit, i, j-1, 2);
                ins=lenA+lenB;
                sub=fillTable(edit, i, j-1, 3);
                skip=fillTable(edit, i, j-1, 0);

                min = minFour(del, ins, sub, skip);
                edit[i][j][k]= 1 + min;
                return edit[i][j][k];

            case 2:
                del=lenA+lenB;
                ins=fillTable(edit, i-1, j, 1);
                sub=fillTable(edit, i-1, j, 3);
                skip=fillTable(edit, i-1, j, 0);

                min = minFour(del, ins, sub, skip);
                edit[i][j][k]= 1 + min;
                return edit[i][j][k];

            case 3:
                del=fillTable(edit, i-1, j-1, 2);
                ins=fillTable(edit, i-1, j-1, 1);
                sub=lenA+lenB;
                skip=fillTable(edit, i-1, j-1, 0);
                
                min = minFour(del, ins, sub, skip);
                edit[i][j][k]= 1 + min;

                return edit[i][j][k];

            default:
                System.out.println("Invalid Operation");
                System.exit(1);
            break;
        }
        return -1;
    }

    public static int minFour(int a, int b, int x, int y){
        if(a<=b && a<=x && a<=y){
            return a;
        }

        if(b<=a && b<=x && b<=y){
            return b;
        }
        
        if(x<=b && x<=a && x<=y){
            return x;
        }

        if(y<=b && y<=x && y<=a){
            return y;
        }
        return lenA+lenB;
    }

//****************
//START: READ ONLY
//****************
 
    public static int EditDistance(String a, String b) {
//****************
//END: READ ONLY
//****************

		//WRITE YOUR NSID: bam857
		
		//start: edit and write your code here
        lenA = a.length();
        lenB = b.length();
        int res;
        aS=a;
        bS=b;

        edit = new int[lenA+1][lenB+1][nOP];
        int i = 0;
        for(i = 0; i<nOP; i++){
            fillTable(edit, lenA, lenB, i);
        }
        
        res=minFour(edit[lenA][lenB][0], edit[lenA][lenB][1], edit[lenA][lenB][2], edit[lenA][lenB][3] );

        if(res == lenA+lenB) return -1;
        return res;
        //end: write your code here 		
    }
//****************
//START: READ ONLY
//****************
    /**
     * Main Function.
     */
    public static void main(String[] args) {

        BufferedReader reader;
        File file = new File("output.txt"); 
		String line;
        try {
            reader = new BufferedReader(new FileReader("edit.txt"));
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));            
            while(true){ 
				line = reader.readLine();
				if(line == null) break;				
				StringTokenizer st = new StringTokenizer(line, ",");
				String a = st.nextToken();
				String b = st.nextToken();
                writer.write(EditDistance(a,b) + "\n");
            } 

            reader.close();
            writer.close();
        } catch (FileNotFoundException e) {
            System.out.println("Could not locate input file.");
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

}
