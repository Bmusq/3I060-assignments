
import java.io.*;
import java.util.StringTokenizer;  
//****************
//START: READ ONLY
//****************
public class Edit_book{
//****************
//END: READ ONLY
//****************
    public static int[][] edit;
    public static int pl;
    public static int pc;

    public static void printEditTable(int[][] edit){
        int i,j;
        for(i=0;i<edit.length;i++){
            for(j=0;j<edit[i].length;j++){
                System.out.print(edit[i][j]+" ");
            }
            System.out.println();
        }

        System.out.println();
    }

    public static boolean bestWay(){
        pc=0;
        pl=0;
        int turn=-1;
        // min{0.pl,pc+1 ; 1.pl+1,pc ; 2.pl+1,pc+1}
        // Incremente pc et pl en consequence
        // variable turn=0;1;2 en fonction du choix
        while(pl<edit.length && pc<edit[0].length){
            // min{ (0 ; 1 ; 2)\turn}
            //
        }



        return false;
    }
//****************
//START: READ ONLY
//****************
 
    public static int EditDistance(String a, String b) {
//****************
//END: READ ONLY
//****************

		//WRITE YOUR NSID: bam857
		
		//start: edit and write your code here 
		int i,j;
        int m=a.length();
        int n=b.length();
        edit= new int[m+1][n+1];
        int ins=0, del=0, rep=0, min=0;
        int pl=0, pc=0;

       /* if(m>n*2){
            return -1;
        }
        if(n>m*2){
            return -1;
        }*/

        for(j=0; j<=n; j++){
            edit[0][j]=j;
        }
        for(i=1; i<=m; i++){
            edit[i][0]=i;
            for(j=1; j<=n; j++){
                // Calculate minimum cost with:
                    /* Insertion */
                    ins=edit[i][j-1]+1;

                    /* Deletion */
                    del=edit[i-1][j]+1;

                    /* Substitution */
                    if(a.charAt(i-1) == b.charAt(j-1)){
                        rep=edit[i-1][j-1];
                    }
                    else{
                        rep=edit[i-1][j-1]+1;
                    }
                // End calcul cost

                if(rep<=del && rep<=ins){
                    min=rep;
                }
                else if(del<=rep && del<=ins){
                    min=del;
                }
                else{
                    min=ins;
                }
                edit[i][j]=min;
            }
        }

        if(bestWay()){
            return -1;
        }

        printEditTable(edit);
		return edit[pl][pc];
        //end: write your code here 		
    }
//****************
//START: READ ONLY
//****************
    /**
     * Main Function.
     */
    public static void main(String[] args) {

        BufferedReader reader;
        File file = new File("output.txt"); 
		String line;
        try {
            reader = new BufferedReader(new FileReader("edit.txt"));
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));            
            while(true){ 
				line = reader.readLine();
				if(line == null) break;				
				StringTokenizer st = new StringTokenizer(line, ",");
				String a = st.nextToken();
				String b = st.nextToken();
                writer.write(EditDistance(a,b) + "\n");
            } 

            reader.close();
            writer.close();
        } catch (FileNotFoundException e) {
            System.out.println("Could not locate input file.");
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

}
