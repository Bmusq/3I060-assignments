import java.io.*;
import java.util.*;

public abstract class Dbam857{
	static String[] extractInput(String path)
	throws IOException{
		String tmp=null;
		File f= new File(path);
		BufferedReader br=null;
		int i;
		int j;
		int nblines=0;
		String[] retvalue=null;

		try{
			try{
				br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			}
			catch (FileNotFoundException fnf){
				System.out.println("File not found: "+fnf);
				System.exit(1);
			}

			tmp=br.readLine();
			for(i=tmp.length()-2, j=0; i>=0; i--, j++){
				nblines=nblines + (tmp.charAt(i)-'0')*((int)Math.pow(10,j));
			}


			retvalue=new String[nblines];
			for(i=0; i<nblines;i++){
				retvalue[i]= new String("");	
			}

			j=0;
			while(nblines>0){
				tmp=br.readLine();
				i=0;
				while(tmp!=null && tmp.charAt(i)!='.'){
					retvalue[j]+=(tmp.charAt(i));
					i++;
				}

				j++;
				nblines --;
			}
		}
		catch (IOException e){
			System.out.println("Reading File Error: "+e);
			System.exit(1);
		}
		finally{
			if(br!=null){
				br.close();
			}
		}
		return retvalue;
	}

	static int[] stringToDigits(String[] digits){
		int[] tab=new int[digits.length];
		int i,j,k;

		for(i=0; i<tab.length;i++){
			tab[i]=0;
			for(j=0, k=(digits[i].length()-1); j<digits[i].length(); j++, k--){
				tab[i]=tab[i]+ ((digits[i].charAt(j) - '0') * ((int) Math.pow(10,k)));
			}
		}
		return tab;
	}

	static int[] initOutput(int[] tabInt){
		int i;
		for(i=0; i<tabInt.length;i++){
			tabInt[i]=softwareRecursion(tabInt[i]);
		}
		return tabInt;
	}

	static int softwareRecursion(int n){
		int[][] tab=new int[2][n+1];
		tab[0][0]=1;
		tab[1][0]=1;
		int i;

		for(i=1; i<=n; i++){
			tab[0][i]=tab[0][i-1]+tab[1][i-1];
			tab[1][i]=tab[0][i]+tab[1][i-1];
		}
		return tab[1][n];
	}

	static void exportInFile(int[] res)
	throws IOException{
		PrintWriter output=null;
		int i;

		output=new PrintWriter("Dbam857.txt");

		for(i=0; i<res.length;i++){
			output.print(res[i]);
			output.println('.');
		}

		if(output != null){
			output.close();
		}
	}

	public static void main(String[] args) {
		try{
			exportInFile(initOutput(stringToDigits(extractInput("./software.txt"))));
		}
		catch(IOException e){
			System.out.println("Error output:" +e);
		}
	}
}