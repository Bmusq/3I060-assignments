import java.io.*;
import java.util.*;

public abstract class Abam857{
	static StringBuilder[] extractInput(String path)
	throws IOException{
		String tmp=null;
		File f= new File(path);
		BufferedReader br=null;
		int i;
		int j;
		int nblines=0;
		StringBuilder[] retvalue=null;

		try{
			try{
				br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			}
			catch (FileNotFoundException fnf){
				System.out.println("File not found:"+fnf);
				System.exit(1);
			}

			tmp=br.readLine();
			for(i=tmp.length()-2, j=0; i>=0; i--, j++){
				nblines=nblines + (tmp.charAt(i)-'0')*((int)Math.pow(10,j));
			}


			retvalue=new StringBuilder[nblines];
			for(i=0; i<nblines;i++){
				retvalue[i]= new StringBuilder("");	
			}

			j=0;
			while(nblines>0){
				tmp=br.readLine();
				i=0;
				while(tmp!=null && tmp.charAt(i)!='.'){
					retvalue[j].append(tmp.charAt(i));
					i++;
				}

				j++;
				nblines --;
			}
		}
		catch (IOException e){
			System.out.println("Reading File Error:"+e);
			System.exit(1);
		}
		finally{
			if(br!=null){
				br.close();
			}
		}
		return retvalue;
	}

	static StringBuilder [] resolvePalindrome(StringBuilder [] digits){
		int i, j, k;
		for(i=0; i<digits.length;i++){
			k=(digits[i].length()) - 1;
			j=0;
			while(k>j){
				if(digits[i].charAt(j)==digits[i].charAt(k)){
					k--;
				}
				else{
					digits[i].insert(k+1, digits[i].charAt(j));
				}
				j++;
			}
		}
		return digits;
	}

	static void exportOutput(StringBuilder [] digits)
	throws IOException{
		PrintWriter output=null;
		int i;
		int j;

		output=new PrintWriter("Abam857.txt");
		
		for(i=0; i<digits.length;i++){
			for(j=0; j<digits[i].length();j++){
				output.print(digits[i].charAt(j));
			}
			output.println('.');
		}

		if(output != null){
			output.close();
		}

	}

	public static void main(String[] args) {
		try{
			exportOutput(resolvePalindrome(extractInput("./palindrome.txt")));
		}
		catch(IOException e){
			System.out.println("Error output:"+e);
		}
	}
}