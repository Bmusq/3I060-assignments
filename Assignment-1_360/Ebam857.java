import java.io.*;
import java.util.*;

public abstract class Ebam857{
	static String[] extractInput(String path)
	throws IOException{
		String tmp=null;
		File f= new File(path);
		BufferedReader br=null;
		int i;
		int j;
		int nblines=0;
		String[] retvalue=null;

		try{
			try{
				br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			}
			catch (FileNotFoundException fnf){
				System.out.println("File not found: "+fnf);
				System.exit(1);
			}

			tmp=br.readLine();
			for(i=tmp.length()-2, j=0; i>=0; i--, j++){
				nblines=nblines + (tmp.charAt(i)-'0')*((int)Math.pow(10,j));
			}


			retvalue=new String[nblines];
			for(i=0; i<nblines;i++){
				retvalue[i]= new String("");	
			}

			j=0;
			while(nblines>0){
				tmp=br.readLine();
				i=0;
				while(tmp!=null && tmp.charAt(i)!='.'){
					retvalue[j]+=(tmp.charAt(i));
					i++;
				}

				j++;
				nblines --;
			}
		}
		catch (IOException e){
			System.out.println("Reading File Error: "+e);
			System.exit(1);
		}
		finally{
			if(br!=null){
				br.close();
			}
		}
		return retvalue;
	}

	static int[] stringToDigits(String[] digits){
		int[] tab=new int[digits.length];
		int i,j,k;

		for(i=0; i<tab.length;i++){
			tab[i]=0;
			for(j=0, k=(digits[i].length()-1); j<digits[i].length(); j++, k--){
				tab[i]=tab[i]+ ((digits[i].charAt(j) - '0') * ((int) Math.pow(10,k)));
			}
		}
		return tab;
	}

	static int[] sequenceResolve(int[] tabInt){
		int i,j;

		for(i=0;i<tabInt.length;i++){
			j=0;
			while(tabInt[i]!=1){
				if(tabInt[i]%2==0)
					tabInt[i]=tabInt[i]/2;
				else
					tabInt[i]=tabInt[i]*3+1;
				j++;
			}
			tabInt[i]=j+1;
		}
		return tabInt;
	}

	static void exportInFile(int[] tabInt)
	throws IOException{
		PrintWriter output=null;
		int i;

		output=new PrintWriter("Ebam857.txt");

		for(i=0; i<tabInt.length;i++){
			output.print(tabInt[i]);
			output.println('.');

		}
		if(output != null){
			output.close();
		}
	}

	public static void main(String[] args) {
		
		try{
			exportInFile(sequenceResolve(stringToDigits(extractInput("./sequence.txt"))));
		}
		catch(IOException e){
			System.out.println("Error output: " +e);
		}
	}
}