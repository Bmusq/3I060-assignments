import java.io.*;
import java.util.*;

public abstract class Cbam857{
	static String[] extractInput(String path)
	throws IOException{
		String tmp=null;
		File f= new File(path);
		BufferedReader br=null;
		int i, j, k;
		int nblines=0;
		String[] retvalue=null;

		try{
			try{
				br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			}
			catch (FileNotFoundException fnf){
				System.out.println("File not found:"+fnf);
				System.exit(1);
			}

			tmp=br.readLine();
			for(i=tmp.length()-2, j=0; i>=0; i--, j++){
				nblines=nblines + (tmp.charAt(i)-'0')*((int)Math.pow(10,j));
			}

			retvalue=new String[nblines*2];
			for(i=0; i<nblines*2;i++){
				retvalue[i]="";	
			}

			j=0;
			while(nblines>0){
				tmp=br.readLine();
				i=0;
				for(k=0; k<2; k++){
					while(tmp!=null && tmp.charAt(i)!='.' && tmp.charAt(i)!=','){
						retvalue[j]+= tmp.charAt(i);
						i++;
					}
					i++;
					j++;
				}					
				nblines --;
			}
		}
		catch (IOException e){
			System.out.println("Reading File Error:"+e);
			System.exit(1);
		}
		finally{
			if(br!=null){
				br.close();
			}
		}
		return retvalue;
	}


	static int[][] stringToDigits(String[] digits){
		int[][] tab=new int[digits.length/2][2];
		int i,j,k,l;

		for(i=0; i<tab.length;i++){
			for(l=0; l<tab[i].length; l++){
				tab[i][l]=0;
				for(j=0, k=(digits[i*2+l].length()-1); j<digits[i*2+l].length(); j++, k--){
					tab[i][l]=tab[i][l]+ ((digits[i*2+l].charAt(j) - '0') * ((int) Math.pow(10,k)));
				}
			}
		}
		return tab;
	}


	static int[] initOutput(int[][] tabInt){
		int i;
		int[] res=new int[tabInt.length];

		for(i=0; i<tabInt.length;i++){
			res[i]=resolveX(tabInt[i][0],tabInt[i][1]);
		}
		return res;
	}

	static int resolveX(int m, int n){
		int a,b;
		if(m==0 || n==0){
			return 0;
		}
		if(!(m%2==n%2)){
			if(m%2==1) m--;
			if(n%2==1) n--;
		}

		if(m<=n){
			a=m--;
			b=n--;
		}else{
			a=n--;
			b=m--;
		}

		return ((a*(a+1))/2) + ( ((b*(b+1))/2) - (((b-a)*(b-a+1))/2) ); 
	}

	static void exportOutput(int[] res)
	throws IOException{
		PrintWriter output=null;
		int i;

		output=new PrintWriter("Cbam857.txt");

		for(i=0; i<res.length;i++){
			output.print(res[i]);
			output.println('.');
		}

		if(output != null){
			output.close();
		}
	}


	public static void main(String[] args) {
		try{
			exportOutput(initOutput(stringToDigits(extractInput("./recursion.txt"))));
		}
		catch(IOException e){
			System.out.println("Error output:"+e);
		}

	}
}