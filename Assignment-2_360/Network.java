
import java.io.*;
import java.util.StringTokenizer;  
//****************
//START: READ ONLY
//****************
public class Network {
//****************
//END: READ ONLY
//****************

// YOU CAN DEFINE YOUR OWN FUNCTIONS HERE IF YOU REALLY NEED ONE

//****************
//START: READ ONLY
//****************

    /**     
	 * @param n : The number of packets
     * @param D : An array representing the packet ordering  
     * @return The performance metric for D
     */
    public static int metric(int []D, int n) {
//****************
//END: READ ONLY
//****************

		//WRITE YOUR NSID: bam857 
		
		//start: edit and write your code here 
        int []s=new int[n];
        int k=0, indmax=0, i;
        s[k]=0;
        for(i=1; i<n; i++){
            if(D[indmax]>D[i]){
                if((D[indmax]-D[i])>s[k]){
                    s[k]=D[indmax]-D[i];
                }
            }
            else{
                k++;
                s[k]=0;
                indmax=i;
            }
        }

        int max=s[0];
        for(i=1; i<n;i++){
            if(max<s[i])
                max=s[i];
        }   

        return max;

        //end: write your code here 
	       
		 
		
    }
//****************
//START: READ ONLY
//****************
    /**
     * Main Function.
     */
    public static void main(String[] args) {

        BufferedReader reader;
        File file = new File("output.txt");
		int n = 0;
		int []X= new int[1000];
		String line;
        try {
            reader = new BufferedReader(new FileReader("Network.txt"));
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));            
            while(true){ 
				line = reader.readLine();
				if(line == null) break;				
				StringTokenizer st = new StringTokenizer(line, ",");
				n = 0;
				while (st.hasMoreTokens()) {  
					X[n] = Integer.parseInt(st.nextToken()); 
					//System.out.println(""+X[n]);					
					n++;
				}
                writer.write(metric(X,n) + "\n");
            } 

            reader.close();
            writer.close();
        } catch (FileNotFoundException e) {
            System.out.println("Could not locate input file.");
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

}
