
import java.io.*;
import java.util.StringTokenizer;  
//****************
//START: READ ONLY
//****************
public class Quantum {
//****************
//END: READ ONLY
//****************
    static int[][] grid;
    static int nbline;
    static int grid_size;
    static int min_size=2;

    public static void init(int x){
        nbline=x;

        grid_size=x*2;

        /*  grid[][0]= x start point
            grid[][1]= y start point
            grid[][2]= x end point
            grid[][3]= y end point
        */
        grid=new int[x][4];

        // Place the first line
        grid[0][0]=0;
        grid[0][1]=0;
        grid[0][2]=grid_size-1;
        grid[0][3]=0;
    }

    public static boolean legitState(int x){
        int i, j;
        int count=0;
        // for each columns
        for(i=0; i<=grid_size; i++){
            // Is each line in it?
            count =0 ;
            for(j=1; j<=x ; j++){
                if(grid[j][0]<=i && i<=grid[j][2])
                    count ++;
            }
            if(count == x){
                return true;
            }
        }
        return false;
      }


    public static int calculateCost(int ind){
        int nbSeg;
        int pos;
        int size=grid[ind][2]-grid[ind][0];
        boolean legitSeg;
        int nbCross;


        int[] seg=new int[4];
        /*
            [0]= x bottom = pos
            [1]= y bottom = nbSeg
            [2]= x top = pos
            [3]= y top = ind
        */

        // The y coord of the top point never changes
        seg[3]= ind;


        int costSeg;
        int currcost;
        int totalcost=0;
        int xL;

        for(nbSeg=0; nbSeg<ind ; nbSeg++){
            seg[1]=nbSeg;
            costSeg=10000;
            for(pos=grid[ind][0]; pos<=size; pos++){
                currcost=0;
                seg[0]=pos;
                seg[2]=pos;
                legitSeg=false;

                for(xL=grid[nbSeg][0]; xL<=grid[nbSeg][2]; xL++){
                    if(xL==pos){
                        legitSeg=true;
                        break;
                    }
                }

                if(!legitSeg) continue;

                for(nbCross=ind-1; nbCross>nbSeg; nbCross--){
                    for(xL=grid[nbCross][0]; xL<=grid[nbCross][2]; xL++){
                        if(xL==pos){
                            currcost++;
                            break ;
                        }
                    }
                    if(currcost>=3) break;
                }

                if(currcost<costSeg)
                    costSeg=currcost;
            }
            totalcost+=costSeg;
        }
        return totalcost;
    }

//****************
//START: READ ONLY
//****************
        
    
    /**     
     * @param n : The number of buses
     * @return The cost of minimum crossing configuration with X buses
     */
    public static int cost(int X) {
//****************
//END: READ ONLY
//****************
        //WRITE YOUR NSID: bam857       
        //start: edit and write your code here
        if(X==0)
            return 0;

        int sx,sy; // Coord start point
        int ex,ey; // Coord end point
        int linecost=10000;
        int currcost;
        int i;

        if(X==1){
            grid[nbline-X][0]=0;
            grid[nbline-X][1]=nbline-X;
            grid[nbline-X][2]=grid_size-1;
            grid[nbline-X][3]=nbline-X;

            linecost=calculateCost(nbline-X);
            return linecost;
        }

        for(sy=nbline-X, sx=0; sx<=((grid_size)-min_size); sx++){
            for(ey=sy, ex=sx+min_size; ex<=grid_size-1; ex++){
                currcost=2;
                // Add the line consider in the grid
                grid[nbline-X][0]=sx;
                grid[nbline-X][1]=sy;
                grid[nbline-X][2]=ex;
                grid[nbline-X][3]=ey;
                // Does the line reaches every other line
                if(!legitState(nbline-X)){ continue;}

               for(i=nbline-X-1; i>0; i--){
                    if(sx==grid[i][0] && ex==grid[i][2]){
                        continue;
                    }
                }

                currcost=calculateCost(nbline-X);
                if(currcost>linecost){continue;}
                currcost+=cost(X-1);

                if(currcost<linecost){
                    linecost=currcost;          
                }
            }
        }
        return linecost;
        //end: write your code here
    }
//****************
//START: READ ONLY
//****************
    /**
     * Main Function.
     */
    public static void main(String[] args) {

        BufferedReader reader;
        File file = new File("output.txt");
        int X = 0; 
        String line;
        try {
            reader = new BufferedReader(new FileReader("Quantum.txt"));
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));            
            while(true){ 
                line = reader.readLine();
                if(line == null) break;             
                X = Integer.parseInt(line);
                init(X);
                writer.write(cost(X-1) + "\n");
                writer.flush();
            } 

            reader.close();
            writer.close();
        } catch (FileNotFoundException e) {
            System.out.println("Could not locate input file.");
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

}
